//
//  AccounInfos.swift
//  DuomBase
//
//  Created by kuroky on 2023/2/16.
//  

import Foundation

public let sitckerRSAKey: String = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALkZz+25MbKEuZ1dzPPlpFiu0TaECyBYU7Rn55KjHLN/c0xacTUyjpTuowXKKB6hddw6eeTyLieNouuYXw+WlRkEEfsqx/INQel2I4tZ+tUL9ovH9SEtZJ0+alOQljjCGYxfRNSltFo9L1HnT/Wog1iv5zSDxeHWaIrRF2M3TL0zAgMBAAECgYEAr6f4M3+o0uGzh8xJQDdwVBxqPBLowCQyIyTrlLOn/At57j5x6mr09g7UtBzZH+BXicwQexz8gdqefndV1uQuzjZWdrPUZyw1ey8YpVS2EbbmgPlDtEkkqf5KjeuHM+wU79mLMeo2AH+dArzgLTbrifXdtvG26LMlyeGpDehb2SkCQQDecvoeR7gtZ5m/IGkYrCYkL4SDaIanpOSixTP0Gfkfy7kp1/CkMwQkkkdOJtgw1SsiIrUHOwO/2Fb7tZDCd43tAkEA1QTF+L4a7Rmmb+e/MVW0KGWmyeZLMw3lUpm3cRiE2vO1fNRHqoWJOroAbXWFHhu9ZcJe5YkTRPrZdtZCZS8TnwJAU/eOlhN4YBs1G8H+KcQBebM+D+RKv8ksTIynucJ8GRZrRHIwcaqYK73TkJp1DKnKaE41iw8psSbWXpkAlYdPWQJADhIfpZuhGbJNDS1IstCPcu+sAcAljXzIfhlLRbwx4migv1siVMA3XVq19oUBwwhgDV22eQEB9deAD9p9TkO6zwJAAjS6h7cHwwCE12ZTUPx/9TS/AuBiOHsM8jrWyrCPDhGAuBlmD0z6JWPJC/05EFxtX2VW3UKB5D6uJmXtolhxCA=="

// MARK: 贴纸信息
public class StickerInfo: NSObject, Codable {
    public var id: String?
    public var celebId: String?
    public var name: String?
    public var icon: String?
    public var desc: String?
    public var image: [StickerImages] = []
    public var priceType: String?
    public var currencyType: String?
    public var price: Double? = 0.0
    public var gateType: String?
    public var gateValue: String?
    public var hasPermission: Bool? = false
    public var purchased: Bool? = false
    public var downloadStatus: Int? // 0:notDownload  1：downloading  2：downloaded
    public var decryptImages: [StickerImages]?
    
    
    public var itemCount: Int?
    
    public var firstFrameImage: [UIImage?]?
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(String.self, forKey: .id)
        self.celebId = try container.decodeIfPresent(String.self, forKey: .celebId)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.icon = try container.decodeIfPresent(String.self, forKey: .icon)
        self.desc = try container.decodeIfPresent(String.self, forKey: .desc)
        self.image = try container.decode([StickerImages].self, forKey: .image)
        self.priceType = try container.decodeIfPresent(String.self, forKey: .priceType)
        self.currencyType = try container.decodeIfPresent(String.self, forKey: .currencyType)
        self.price = try container.decodeIfPresent(Double.self, forKey: .price) ?? 0.0
        self.gateType = try container.decodeIfPresent(String.self, forKey: .gateType)
        self.gateValue = try container.decodeIfPresent(String.self, forKey: .gateValue)
        self.hasPermission = try container.decodeIfPresent(Bool.self, forKey: .hasPermission)
        self.purchased = try container.decodeIfPresent(Bool.self, forKey: .purchased)
        self.downloadStatus = try container.decodeIfPresent(Int.self, forKey: .downloadStatus)
        self.decryptImages = try container.decodeIfPresent([StickerImages].self, forKey: .decryptImages) ?? []

        self.itemCount = self.image.count
    }
    
    enum CodingKeys: String, CodingKey {
        case desc = "description"
        
        case id
        case celebId
        case name
        case icon
        case image
        case priceType
        case currencyType
        case price
        case gateType
        case gateValue
        case hasPermission
        case purchased
        case downloadStatus
        case decryptImages
    }
    
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(celebId, forKey: .celebId)
        try container.encode(name, forKey: .name)
        try container.encode(icon, forKey: .icon)
        try container.encode(desc, forKey: .desc)
        try container.encode(image, forKey: .image)
        try container.encode(priceType, forKey: .priceType)
        try container.encode(currencyType, forKey: .currencyType)
        try container.encode(price, forKey: .price)
        try container.encode(gateType, forKey: .gateType)
        try container.encode(gateValue, forKey: .gateValue)
        try container.encode(hasPermission, forKey: .hasPermission)
        try container.encode(purchased, forKey: .purchased)
        try container.encode(downloadStatus, forKey: .downloadStatus)
    }
    
}

public class StickerImages: NSObject, Codable {
    public var original: String?
    public var firstFrame: String?
    public var decryptOriginal: String?
    public var decryptFirstFrame: String?

}
