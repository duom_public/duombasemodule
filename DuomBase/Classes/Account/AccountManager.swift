//
//  AccountManager.swift
//  Differentiator
//
//  Created by kuroky on 2022/10/14.
//

import Foundation

// MARK: 用户信息
public struct Account: Codable {
    public var isPhoneCertified: Bool?
    public var isGoogleCertified: Bool?
    public var wsKey: String?
    public var isBindGoogle: Bool?
    public var nickName: String?
    public var isBindEmail: Bool?
    public var updateTime: String?
    public var isEmailCertified: Bool?
    public var registerType: Int?
    public var avatar: String?
    public var userName: String?
    public var locale: String?
    public var vipEndTime: String?
    public var isExistAssetPassword: Bool?
    public var vipLevel: String?
    public var isBindPhone: Bool?
    public var phone: String?
    public var isMigration: Int?
    public var idCertifiedStatus: Int?
    public var id: String?
    public var jwtKey: String?
    public var email: String?
    public var status: Int?
    public var market_push: Bool?
    
    // 下面是同步信息后添加的字段
    public var gender: String?
    public var intro: String?
    
}

// 用户更新信息后返回的数据结构
public struct SyncAccount: Codable {
    var customerId: String?
    var intro: String?
    var nickName: String?
    var avatar: String?
    var gender: String?
}


public final class AccountManager {
    public static let shared = AccountManager()
    
    private var _account: Account?
    
    public var currentAccount: Account? {
        return _account
    }
    
//    private var _stickers: [StickerInfo] = []
//
//    public var currentStickers: [StickerInfo] {
//        return _stickers
//    }
    
    private init() {
        if let data = UserDefaults.standard.data(forKey: UserDefaults.Key.account), let account = try? JSONDecoder().decode(Account.self, from: data) {
            _account = account
        } else {
            _account = nil
        }
    }
    
    /// 当前登录状态
    public var isLogined: Bool {
        if let account = currentAccount,
           let jwtToken = account.jwtKey, !jwtToken.isEmpty,
           let userId = account.id, !userId.isEmpty {
            return true
        }
        return false
    }
    
    // set
    public func saveAccount()  {
        if let data = UserDefaults.standard.data(forKey: UserDefaults.Key.account), let account = try? JSONDecoder().decode(Account.self, from: data) {
            _account = account
        } else {
            _account = nil
#if DEBUG
                toast("userInfo is empty!!!")
#endif
        }
    }
    
    // login out  --> clear
    public func accountDidLogout() {
        UserDefaults.standard.set(nil, forKey: UserDefaults.Key.account)
        UserDefaults.standard.synchronize()
        
        _account = nil
    }
    
    // update
    public func updateAccount(_ data: Data) {
        if let syncAccount = try? JSONDecoder().decode(SyncAccount.self, from: data) {
            _account?.avatar = syncAccount.avatar
            _account?.id = syncAccount.customerId
            _account?.nickName = syncAccount.nickName
            _account?.gender = syncAccount.gender
            _account?.intro = syncAccount.intro
        } else {
#if DEBUG
            toast("update userInfo failure")
#endif
        }
    }
}

// MARK: 用户关联的其他信息
// 贴纸
extension AccountManager {
    public func getStickersInfo() -> [StickerInfo] {
        if let account = self.currentAccount,
           let currentId = account.id,
           let infoString = UserDefaults.standard.value(forKey: currentId) as? String,
           let data = infoString.data(using: .utf8, allowLossyConversion: false),
           let stickers = try? JSONDecoder().decode([StickerInfo].self, from: data) {
            return stickers
        } else {
#if DEBUG
                toast("stickerInfo is empty!")
#endif
            return [StickerInfo]()
        }
    }
    
}


