//
//  DuomUserDefaults.swift
//  Differentiator
//
//  Created by kuroky on 2022/10/14.
//

import Foundation

public extension UserDefaults {
    enum Key {
        public static let account = "accountKey"
        public static let accountUpdate = "updateAccountKey"
        public static let userName = "userName"
        public static let userId = "userId"
        public static let deviceId = "deviceId"
        public static let token = "JWT_TOKEN"
        
        
    }
}

extension UserDefaults {
    public func setCodabelObject<T: Codable>(_ object: T, forKey key: String, usingEncoder encoder: JSONEncoder = JSONEncoder())  {
        let data = try? encoder.encode(object)
        set(data, forKey: key)
    }
    
    public func getCodabelObject<T: Codable>(_ type: T.Type, forKey key: String, usingDecoder decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = value(forKey: key) as? Data else { return nil }
        return try? decoder.decode(type.self, from: data)
    }
}
