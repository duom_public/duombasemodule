//
//  NSSwiftLoader.h
//  VCSubModule
//
//  Created by kuroky on 2022/11/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol NSSwiftLoadProtocol

+ (void)swiftLoad;

@end

@interface NSSwiftLoader : NSObject

@end

NS_ASSUME_NONNULL_END
