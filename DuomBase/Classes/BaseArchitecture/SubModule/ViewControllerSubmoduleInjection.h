//
//  ViewControllerSubmoduleInjection.h
//  DuomBase
//
//  Created by kuroky on 2022/11/16.
//

#import <Foundation/Foundation.h>
@import UIKit;

NS_ASSUME_NONNULL_BEGIN

typedef void(^PerfromWithDestination)(id, id);

@interface ViewControllerSubmoduleInjection : NSObject

+ (void)setPerfromWithDestination:(PerfromWithDestination)block;

@end

NS_ASSUME_NONNULL_END
