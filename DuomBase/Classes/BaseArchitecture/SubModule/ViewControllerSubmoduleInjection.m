//
//  ViewControllerSubmoduleInjection.m
//  DuomBase
//
//  Created by kuroky on 2022/11/16.
//

#import "ViewControllerSubmoduleInjection.h"
@import ObjectiveC.runtime;

@interface SubmoduleSharedInjection : NSObject
@property (nonatomic, copy) PerfromWithDestination perfromWithDestination;
+ (instancetype)shareInstance;
@end

@implementation SubmoduleSharedInjection

static SubmoduleSharedInjection *_instance = nil;

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

@end


@implementation ViewControllerSubmoduleInjection

+ (void)methodSwizzle:(Class)originClass originSelector: (SEL)originSelector toClass:(Class)swizzleClass swizzleSelector:(SEL)swizzleSelector {
    Method originMethod = class_getInstanceMethod(originClass, originSelector);
    Method swizzlingMethod = class_getInstanceMethod(swizzleClass, swizzleSelector);
    IMP originIMP = method_getImplementation(originMethod);
    IMP swizzleIMP = method_getImplementation(swizzlingMethod);
    class_addMethod(originClass, swizzleSelector, originIMP, method_getTypeEncoding(originMethod));
    BOOL c = class_addMethod(originClass, originSelector, swizzleIMP, method_getTypeEncoding(swizzlingMethod));
    if (!c) {
        method_exchangeImplementations(originMethod, swizzlingMethod);
    } else {
        class_replaceMethod(originClass, swizzleSelector, originIMP, method_getTypeEncoding(originMethod));
    }
}

+ (void)load {
    [self methodSwizzle:UIViewController.class
         originSelector:NSSelectorFromString(@"_presentViewController:animated:completion:")
                toClass:[self class]
        swizzleSelector:@selector(__presentViewController:animated:completion:)];
    
    [self methodSwizzle:UINavigationController.class
         originSelector:@selector(pushViewController:animated:)
                toClass:[self class]
        swizzleSelector:@selector(__pushViewController:animated:)];
    
    [self methodSwizzle:UIViewController.class
         originSelector:@selector(addChildViewController:)
                toClass:[self class]
        swizzleSelector:@selector(__addChildViewController:)];
}


- (void)__presentViewController:(id)arg1 animated:(BOOL)arg2 completion:(id)arg3 {
    if ([SubmoduleSharedInjection shareInstance].perfromWithDestination != nil && arg1 != nil) {
        [SubmoduleSharedInjection shareInstance].perfromWithDestination(self, arg1);
    }
    [self __presentViewController:arg1 animated:arg2 completion:arg3];
}

- (void)__pushViewController:(UIViewController *)arg1 animated:(BOOL)arg2 {
    if ([SubmoduleSharedInjection shareInstance].perfromWithDestination != nil && arg1 != nil) {
        [SubmoduleSharedInjection shareInstance].perfromWithDestination(self, arg1);
    }
    [self __pushViewController:arg1 animated:arg2];
}

- (void)__addChildViewController:(UIViewController *)arg1 {
    if ([SubmoduleSharedInjection shareInstance].perfromWithDestination != nil && arg1 != nil) {
        [SubmoduleSharedInjection shareInstance].perfromWithDestination(self, arg1);
    }
    [self __addChildViewController:arg1];
}

+ (void)setPerfromWithDestination:(PerfromWithDestination)block {
    [SubmoduleSharedInjection shareInstance].perfromWithDestination = block;
}

@end
