//
//  BaseNavigationController.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/1.
//

import UIKit

public protocol NotHidesBottomBarWhenPushed {}

public typealias NavigationController = BaseNavigationController

open class BaseNavigationController: UINavigationController {
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        // UINavigationBar
        UINavigationBar.appearance().barStyle = .default
        UINavigationBar.appearance().tintColor = .black // 在 iOS9 这个属性影响 UIBarButtonItem 颜色
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().backgroundColor = .white
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.black, .font: UIFont.systemFont(ofSize: 18, weight: .medium)]

        // UIBarButtonItem
        UIBarButtonItem.appearance().tintColor = UIColor.black
        UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.black, .font: UIFont.systemFont(ofSize: 14)], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.black, .font: UIFont.systemFont(ofSize: 14)], for: .highlighted)
        UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.black, .font: UIFont.systemFont(ofSize: 14)], for: .selected)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    override open func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if children.count > 0 {
            if let _ = viewController as? NotHidesBottomBarWhenPushed {
                viewController.hidesBottomBarWhenPushed = false
            } else {
                viewController.hidesBottomBarWhenPushed = true
            }
        }

        // iOS14以后，长按弹出视图菜单问题
        if #available(iOS 14.0, *) {
            viewController.navigationItem.backBarButtonItem = BackBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        } else {
            viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        
        super.pushViewController(viewController, animated: animated)
    }
}

private class BackBarButtonItem: UIBarButtonItem {
    @available(iOS 14.0, *)
    override var menu: UIMenu? {
        set{
            /*
             Don't set the menu here
             super.menu = menu
             */
        }
        get{
            return super.menu
        }
    }
}
