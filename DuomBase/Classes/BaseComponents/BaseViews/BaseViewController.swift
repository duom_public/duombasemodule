//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/19.
//

import UIKit
import RxSwift

open class BaseViewController: UIViewController {

    open var disposeBag = DisposeBag()
    
    open private(set) var didSetupConstraints = false
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.setNeedsUpdateConstraints()
        
    }
    
    override open func updateViewConstraints() {
        if !didSetupConstraints {
            setupConstraints()
            didSetupConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    /// 更新约束
    ///
    /// - Note: 子类重写这个方法，用于给当前view controller 的 view 配置布局
    ///
    open func setupConstraints() {
        // Override point
    }

}

