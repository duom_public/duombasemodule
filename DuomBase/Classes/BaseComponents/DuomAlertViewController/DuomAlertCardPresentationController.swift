//
//  DuomAlertCardPresentationController.swift
//  DuomBase
//
//  Created by kuroky on 2023/6/19.
//

import Foundation
import UIKit

internal final class DuomAlertCardPresentationController: UIPresentationController {
    var dimmingView: UIView?
    var dimmingViewDidTapped: (() -> Void)?
    
    var presentationWrappingView: UIView?
    
    override var presentedView: UIView? {
        return presentationWrappingView
    }
    
    var enableGesture: Bool = true
    
    convenience init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, enableGesture: Bool = true) {
        self.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        self.enableGesture = enableGesture
    }
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }
    
    override func presentationTransitionWillBegin() {
        
        guard let presentedViewControllerView = super.presentedView else { return }
        
        // presentationWrapperView              <- shadow
        //   |- presentationRoundedCornerView   <- rounded corners (masksToBounds)
        //        |- presentedViewControllerView (presentedViewController.view)
        
        let presentationWrapperView = UIView(frame: frameOfPresentedViewInContainerView).then {
            $0.backgroundColor = UIColor.clear
            $0.layer.shadowColor = UIColor.black.cgColor
            $0.layer.shadowRadius = 5
            $0.layer.shadowOpacity = 0.4
            $0.layer.shadowOffset = CGSize(width: 0, height: 0)
            $0.layer.cornerRadius = 10
        }
        
        let presentationRoundedCornerView = UIView(frame: presentationWrapperView.bounds).then {
            $0.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            $0.backgroundColor = .clear
            $0.layer.cornerRadius = 16
            $0.layer.masksToBounds = true
        }
        
        presentedViewControllerView.frame = presentationWrapperView.bounds
        presentedViewControllerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        presentationWrapperView.addSubview(presentationRoundedCornerView)
        presentationRoundedCornerView.addSubview(presentedViewControllerView)
        
        self.presentationWrappingView = presentationWrapperView
        
        guard let containerView = containerView else { return }
        
        dimmingView = UIView(frame: containerView.bounds).then {
            $0.alpha = 0
            $0.isOpaque = false
            $0.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dimmingViewTapped(_:))))
        }
        
        containerView.insertSubview(dimmingView!, at: 0)
        
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView?.alpha = 1.0
        }, completion: nil)
    }
    
    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView?.alpha = 0.0
        }, completion: nil)
    }
    
    @objc func dimmingViewTapped(_ sender: UITapGestureRecognizer) {
        if enableGesture {
            dimmingViewDidTapped?()
            presentingViewController.dismiss(animated: true, completion: nil)
        }
    }
}

extension DuomAlertCardPresentationController {
    override func presentationTransitionDidEnd(_ completed: Bool) {
        if !completed {
            dimmingView = nil
            presentationWrappingView = nil
        }
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            dimmingView = nil
            presentationWrappingView = nil
        }
    }
}

// MARK: Layout
extension DuomAlertCardPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerViewBounds = containerView?.bounds else { return .zero }

        return calculateScreenCenterRect(containerViewBounds)
    }
    
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        
        guard let containerView = containerView else { return }
        
        dimmingView?.frame = containerView.bounds
        
        presentationWrappingView?.frame = frameOfPresentedViewInContainerView
    }
    
    func calculateScreenCenterRect(_ containerViewBounds: CGRect) -> CGRect {
        let width = containerViewBounds.width - 48 * 2
        let height = max(presentedViewController.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height, DuomAlertCardViewController.cardAlertMinHeight)
        let y = 241.DScale
        let x = (containerViewBounds.width - width) * 0.5
        
        
        return CGRect(x: x, y: y, width: width, height: height)
    }
}
