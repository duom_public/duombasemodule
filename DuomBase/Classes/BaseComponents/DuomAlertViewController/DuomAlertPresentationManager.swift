//
//  DuomAlertPresentationManager.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/15.
//

import UIKit

@objc public enum DuomAlertTransitionStyle: Int {
    case zoomIn
    case fadeIn
}

internal final class DuomAlertPresentationManager: NSObject, UIViewControllerTransitioningDelegate {
    var transitionStyle: DuomAlertTransitionStyle
    
    var enableGesture: Bool
    
    var isCard: Bool
    
    init(transitionStyle: DuomAlertTransitionStyle, enableGesture: Bool = true, isCard: Bool = false) {
        self.transitionStyle = transitionStyle
        self.enableGesture = enableGesture
        self.isCard = isCard
        super.init()
    }

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return isCard ? DuomAlertCardPresentationController(presentedViewController: presented, presenting: source, enableGesture: enableGesture) :  DuomAlertPresentationController(presentedViewController: presented, presenting: source, enableGesture: enableGesture)
    }

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch transitionStyle {
        case .zoomIn:
            return DuomAlertZoomTransition()
        case .fadeIn:
            return DuomAlertFadeTransition()
        }
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch transitionStyle {
        case .zoomIn:
            return DuomAlertZoomTransition()
        case .fadeIn:
            return DuomAlertFadeTransition()
        }
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
}

