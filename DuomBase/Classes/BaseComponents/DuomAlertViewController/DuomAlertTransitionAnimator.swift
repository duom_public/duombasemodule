//
//  DuomAlertTransitionAnimator.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/15.
//

import Foundation

internal class DuomAlertTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    var to: UIViewController!
    var from: UIViewController!
    
    let duration: TimeInterval
    
    init(duration: TimeInterval) {
        self.duration = duration
        super.init()
    }
    
    internal func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    internal func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let to = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let from = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        else { return }
        
        self.to = to
        self.from = from
        
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to) ?? to.view!
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) ?? from.view!
        
        if to.isBeingPresented {
            // Presentation
            
            let container = transitionContext.containerView
            container.addSubview(toView)
            presentedAnimateTransition(from: from, fromView: fromView, to: to, toView: toView, context: transitionContext)
        }
        
        if from.isBeingDismissed {
            // Dismissal
            dismissalAnimateTransition(from: from, fromView: fromView, to: to, toView: toView, context: transitionContext)
        }
    }
    
    public func presentedAnimateTransition(from: UIViewController, fromView: UIView, to: UIViewController, toView: UIView, context: UIViewControllerContextTransitioning) {}
    
    public func dismissalAnimateTransition(from: UIViewController, fromView: UIView, to: UIViewController, toView: UIView, context: UIViewControllerContextTransitioning) {}
}

/// 缩放
internal final class DuomAlertZoomTransition: DuomAlertTransitionAnimator {
    init() {
        super.init(duration: 0.22)
    }

    override func presentedAnimateTransition(from: UIViewController, fromView: UIView, to: UIViewController, toView: UIView, context: UIViewControllerContextTransitioning) {
        toView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [.curveEaseOut], animations: {
            toView.transform = CGAffineTransform(scaleX: 1, y: 1)
        }, completion: { _ in
            context.completeTransition(true)
        })
    }

    override func dismissalAnimateTransition(from: UIViewController, fromView: UIView, to: UIViewController, toView: UIView, context: UIViewControllerContextTransitioning) {
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveEaseIn], animations: {
            fromView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            fromView.alpha = 0.0
        }, completion: { _ in
            context.completeTransition(!context.transitionWasCancelled)
        })
    }
}

/// 淡入淡出
internal final class DuomAlertFadeTransition: DuomAlertTransitionAnimator {
    init() {
        super.init(duration: 0.22)
    }

    override func presentedAnimateTransition(from: UIViewController, fromView: UIView, to: UIViewController, toView: UIView, context: UIViewControllerContextTransitioning) {
        toView.alpha = 0
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveEaseOut], animations: {
            toView.alpha = 1
        }, completion: { _ in
            context.completeTransition(true)
        })
    }

    override func dismissalAnimateTransition(from: UIViewController, fromView: UIView, to: UIViewController, toView: UIView, context: UIViewControllerContextTransitioning) {
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveEaseIn], animations: {
            fromView.alpha = 0.0
        }, completion: { _ in
            context.completeTransition(!context.transitionWasCancelled)
        })
    }
}
