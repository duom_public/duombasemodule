//
//  SyncScrollContainer.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/29.
//

import Foundation

/// 横向滑动容器
public protocol SyncScrollContainer: AnyObject {
    /// 容器内所有的items需要滚动到顶部滚动
    func resetContainerItems(_ resetItem: (SyncInnerScroll) -> ())
}

