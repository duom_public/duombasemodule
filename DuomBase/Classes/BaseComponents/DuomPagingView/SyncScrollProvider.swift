//
//  SyncScrollProvider.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/29.
//

import Foundation
import UIKit

public protocol SyncScrollProvider: AnyObject {
    var scrollView: UIScrollView { get }
}

extension SyncScrollProvider where Self: UIScrollView {
    public var scrollView: UIScrollView { return self }
}

/// 内部需要遵循此协议
public protocol SyncInnerScroll: SyncScrollProvider {}

/// inner提供者 如果提供的UIScrollView可能是UIViewController
public protocol SyncScrollInnerProvider {
    var syncInner: SyncInnerScroll { get }
}

/// 外部需要遵循此协议 - 外部需要重写UIGestureRecognizerDelegate： shouldRecognizeSimultaneouslyWith
public protocol SyncOuterScroll: SyncScrollProvider {
    func wrapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
}

public extension SyncOuterScroll {
    /// 包装一层 - 无法通过协议扩展UIGestureRecognizerDelegate
    func wrapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let isEqualView = gestureRecognizer.view?.isEqual(otherGestureRecognizer.view) ?? false
        if otherGestureRecognizer.view?.isInnerItem == true || isEqualView {
            return true
        }
        return false
    }
}

fileprivate extension UIView {
    var isInnerItem: Bool {
        return self is SyncInnerScroll
    }
}
