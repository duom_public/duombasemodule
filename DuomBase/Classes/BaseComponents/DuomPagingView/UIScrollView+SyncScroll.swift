//
//  UIScrollView+SyncScroll.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/29.
//

import UIKit
import RxSwift
import RxCocoa

public extension Reactive where Base: UIScrollView {
    var kvo_contentSize: Observable<CGSize> {
        base.rx
            .observeWeakly(CGSize.self, #keyPath(UIScrollView.contentSize))
            .compactMap { $0 }
    }
    
    var kvo_contentOffset: Observable<CGPoint> {
        base.rx
            .observeWeakly(CGPoint.self, #keyPath(UIScrollView.contentOffset))
            .compactMap { $0 }
    }
    
    var kvo_delegate: Observable<UIScrollViewDelegate?> {
        base.rx
            .observeWeakly(UIScrollViewDelegate.self, #keyPath(UIScrollView.delegate))
    }
}


extension UIScrollView {
    var sync_minY: CGFloat {
        var minY: CGFloat = -self.contentInset.top
        if #available(iOS 11.0, *) {
            minY = -self.adjustedContentInset.top
        }
        return minY
    }
}
