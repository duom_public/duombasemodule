//
//  DuomSegmentBaseCell.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/28.
//

import Foundation

open class DuomSegmentBaseCell: UICollectionViewCell {
    open var itemModel: DuomSegmentBaseItemModel?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: 子类重写
    open func refresh(_ itemModel: DuomSegmentBaseItemModel?)  {
        self.itemModel = itemModel
    }
}

open class DuomSegmentBaseItemModel {
    /// 设置内容的宽度
    open var contentWidth: CGFloat = 0
    /// 动态宽度， 设置此值 则cell会动态变化宽度， 未设置则取 contentWidth
    open var dynamicWidth: CGFloat?
    
    /// 当前宽度
    lazy var realWidth: CGFloat = {
       return startWidth.transfer(to: endWidth, by: percent)
    }()
    
    var startWidth: CGFloat {
        return contentWidth
    }
    
    var endWidth: CGFloat {
        return dynamicWidth ?? contentWidth
    }
    
    /// 当前进度
    var percent: CGFloat = 0 {
        didSet {
            if let dynamicWidth = self.dynamicWidth {
                realWidth = startWidth.transfer(to: dynamicWidth, by: percent)
            }
        }
    }
}
