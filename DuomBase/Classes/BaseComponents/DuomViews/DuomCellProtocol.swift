//
//  DuomCellProtocol.swift
//  Duom
//
//  Created by kuroky on 2022/9/13.
//

import Foundation
import UIKit

public protocol DuomCellProtocol where Self: UIView {
    static var identifier: String { get set }
}
