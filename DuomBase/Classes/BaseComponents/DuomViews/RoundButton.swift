//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import Foundation
import UIKit

public class RoundButton: UIButton {
    
    public var borderColor: UIColor?
    
    public var radius: CGFloat?
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        rounded(color: borderColor, borderWidth: 1, radius: radius)
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.alpha = 0.65
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.alpha = 1
    }
    
    public override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        self.alpha = 1
    }
}
