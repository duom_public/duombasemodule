//
//  Notifications.swift
//  Duom
//
//  Created by HK on 2019/6/14.
//  Copyright © 2019 butcheryl. All rights reserved.
//

import ObjectiveC
import UIKit
import UserNotifications

class NotificationsProposer: Proposer {
    var currentStatus: Permission.Status {
        if #available(iOS 10.0, *) {
            return statusUserNotifications
        }
        
        return statusUINotifications
    }
    
    var callback: Permission.Callback?
    
    var options: Permission.NotificationOptions
    
    required init(options: Permission.NotificationOptions) {
        self.options = options
    }
    
    func proposeToAccess(_ callback: @escaping Permission.Callback) {
        self.callback = callback
        
        if #available(iOS 10.0, *) {
            requestUserNotifications(callback)
        } else {
            requestUINotifications(callback)
        }
    }
}

@available(iOS, introduced: 8.0, deprecated: 10.0)
internal extension NotificationsProposer {
    fileprivate var statusUINotifications: Permission.Status {
        if let settings = UIApplication.shared.currentUserNotificationSettings, !settings.types.isEmpty {
            return .authorized
        }
        
        return UserDefaults.standard.requestedNotifications ? .denied : .notDetermined
    }
    
    fileprivate func requestUINotifications(_ callback: Permission.Callback) {
        let settings = options
        
        var realSettings: UIUserNotificationType = []
        
        if settings.contains(.alert) {
            realSettings.insert(.alert)
        }
        if settings.contains(.badge) {
            realSettings.insert(.badge)
        }
        if settings.contains(.sound) {
            realSettings.insert(.sound)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(requestingUINotifications), name: UIApplication.willResignActiveNotification, object: nil)
        
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: realSettings, categories: nil))
    }
    
    @objc private dynamic func requestingUINotifications() {
        // 如果0.5秒后还没有弹出系统授权框，手动执行检测授权状态状态
        NSObject.perform(#selector(finishedRequestingNotifications), with: nil, afterDelay: 0.5)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(finishedRequestingNotifications), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc private dynamic func finishedRequestingNotifications() {
        // 如果在0.5秒内检测到有授权弹框则取消之前设置的手动检测
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        
        UserDefaults.standard.requestedNotifications = true
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.callback?(self.statusUINotifications)
        }
    }
}

@available(iOS 10.0, *)
private extension NotificationsProposer {
    var statusUserNotifications: Permission.Status {
        return synchronousStatusUserNotifications
    }
    
    func requestUserNotifications(_ callback: @escaping Permission.Callback) {
        let settings = options
        
        var realSettings: UNAuthorizationOptions = []
        
        if settings.contains(.alert) {
            realSettings.insert(.alert)
        }
        
        if settings.contains(.badge) {
            realSettings.insert(.badge)
        }
        
        if settings.contains(.sound) {
            realSettings.insert(.sound)
        }
        
        if settings.contains(.carPlay) {
            realSettings.insert(.carPlay)
        }
        
        if #available(iOS 12.0, *), settings.contains(.criticalAlert) {
            realSettings.insert(.criticalAlert)
        }
        
        if #available(iOS 12.0, *), settings.contains(.provisional) {
            realSettings.insert(.provisional)
        }
        
        if #available(iOS 12.0, *), settings.contains(.providesAppNotificationSettings) {
            realSettings.insert(.providesAppNotificationSettings)
        }
        
        var status: Permission.Status = .notDetermined
        
        UNUserNotificationCenter.current().requestAuthorization(options: realSettings) { isGranted, error in
            if error != nil {
                status = .denied
            } else {
                status = isGranted ? .authorized : .denied
            }
            callback(status)
        }
    }
    
    private var synchronousStatusUserNotifications: Permission.Status {
        let semaphore = DispatchSemaphore(value: 0)
        
        var status: Permission.Status = .notDetermined
        
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .authorized:
                status = .authorized
            case .denied:
                status = .denied
            case .notDetermined:
                status = .notDetermined
            case .provisional:
                status = .authorized
            @unknown default:
                status = .disabled
            }
            semaphore.signal()
        }
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return status
    }
}
