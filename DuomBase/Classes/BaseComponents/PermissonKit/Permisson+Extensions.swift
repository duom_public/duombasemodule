//
//  Extensions.swift
//  Duom
//
//  Created by HK on 2019/6/14.
//  Copyright © 2019 butcheryl. All rights reserved.
//

import UIKit

extension Bundle {
    var name: String {
        return object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
    }
}

internal extension String {
    static let locationAlwaysUsageDescription = "NSLocationAlwaysUsageDescription"
    static let locationAlwaysAndWhenInUseDescription = "NSLocationAlwaysAndWhenInUseUsageDescription"
    static let locationWhenInUseUsageDescription = "NSLocationWhenInUseUsageDescription"

    static let cameraUsageDescription = "NSCameraUsageDescription"
    static let photoLibraryUsageDescription = "NSPhotoLibraryUsageDescription"

    static let requestedNotifications = "permission.requestedNotifications"
    static let requestedLocationAlwaysWithWhenInUse = "permission.requestedLocationAlwaysWithWhenInUse"
}

extension UserDefaults {
    var requestedNotifications: Bool {
        get { return bool(forKey: .requestedNotifications) }
        set { set(newValue, forKey: .requestedNotifications) }
    }
}
