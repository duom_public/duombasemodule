//
//  DuomNormalRefreshFooter.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import MJRefresh
import UIKit

open class DuomNormalRefreshFooter: DuomBaseRefreshFooter {
    open var style: RefreshStyle = .dark
    
    @objc open override var state: MJRefreshState {
        didSet {
            if state == .noMoreData {
                stateLabel.attributedText = "我们是有底线的".attributes {
                    $0.font(.appFont(ofSize: 14, fontType: .Inter_Medium)).textColor(style == .dark ? UIColor(hex: "0x5A5A5A") : UIColor.white)
                }
            } else if state == .refreshing {
                stateLabel.attributedText = "加载中...".attributes {
                    $0.font(.appFont(ofSize: 12,  fontType: .Inter_Medium)).textColor(style == .dark ? UIColor(hex: "0x27272A") : UIColor.white)
                }
            } else {
                stateLabel.attributedText = nil
            }
            
            if state == .noMoreData || state == .idle {
                loadingView.stopAnimating()
            } else {
                loadingView.startAnimating()
            }
        }
    }
    
    override open func scrollViewContentOffsetDidChange(_ change: [AnyHashable: Any]!) {
        super.scrollViewContentOffsetDidChange(change)
        
        guard state == .idle, mj_y != 0 else { return }
        
        if checkShouldTriggerRefresh() {
            // 防止手松开时连续调用
            let old = (change["old"] as? NSValue)?.cgPointValue ?? .zero
            let new = (change["new"] as? NSValue)?.cgPointValue ?? .zero
            
            guard new.y > old.y else { return }
            
            beginRefreshing()
        }
    }
}
