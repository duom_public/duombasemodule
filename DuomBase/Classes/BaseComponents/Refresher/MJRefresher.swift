//
//  MJRefresher.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import MJRefresh

public protocol Refreshable: AnyObject {
    typealias RefreshHeaderCallbakc = (Refresher) -> Void
    typealias RefreshFooterCallbakc = (Refresher) -> Void
    typealias RefreshComponentCallbakc = (RefreshType, Refresher) -> Void

    var refresher: Refresher? { get }
    
    func setupHeader(_ block: @escaping RefreshHeaderCallbakc)
    func setupFooter(_ block: @escaping RefreshFooterCallbakc)
    func setupHeaderAndFooter(_ block: @escaping RefreshComponentCallbakc)
}

public extension Refreshable {
    var refresher: Refresher? { return nil }
    
    func setupHeader(_ block: @escaping RefreshHeaderCallbakc) {}
    func setupFooter(_ block: @escaping RefreshFooterCallbakc) {}
    func setupHeaderAndFooter(_ block: @escaping RefreshComponentCallbakc) {}
}

extension UIScrollView: Refreshable {}



public final class MJRefresher: Refresher {
    private let darkColor = UIColor(hex: "0x27272A")
    private let lightColor = UIColor.white
    
    private var styles: [RefreshType: RefreshStyle] = [:]
    
    public weak var header: MJRefreshHeader? {
        didSet { updateRefresher() }
    }
    
    public weak var footer: MJRefreshFooter? {
        didSet { updateRefresher() }
    }
    
    public var isRefreshing: Bool { return isRefreshing(with: .header) || isRefreshing(with: .footer) }
    
    public func setting(style: RefreshStyle, type: RefreshType) {
        styles[type] = style
        updateRefresher()
    }
    
    public func endRefresh(with state: RefreshState) {
        if let header = header, header.isRefreshing {
            header.endRefreshing()
        }
        
        switch state {
        case .empty, .noMore:
            if let footer = footer {
                footer.endRefreshingWithNoMoreData()
            }
        case .normal:
            if let footer = footer {
                if footer.isRefreshing {
                    footer.endRefreshing()
                } else {
                    footer.resetNoMoreData()
                }
            }
        }
    }
    
    public func beginRefresh(with type: RefreshType) {
        switch type {
        case .header:
            header?.beginRefreshing()
        case .footer:
            footer?.beginRefreshing()
        }
    }
    
    public func isRefreshing(with type: RefreshType) -> Bool {
        switch type {
        case .header:
            return header?.isRefreshing ?? false
        case .footer:
            return footer?.isRefreshing ?? false
        }
    }
    
    
    fileprivate init(header: MJRefreshHeader?, footer: MJRefreshFooter?) {
        self.header = header
        self.footer = footer
    }
    
    private func updateRefresher()  {
        if let header = header as? MJRefreshNormalHeader, let style = styles[.header] {
            switch style {
            case .dark:
                header.stateLabel?.textColor = darkColor
                header.arrowView?.tintColor = darkColor
                header.loadingView?.style = .gray
            case .light:
                header.stateLabel?.textColor = lightColor
                header.arrowView?.tintColor = lightColor
                header.loadingView?.style = .white
            }
        }
        
        if let footer = footer as? DuomNormalRefreshFooter, let style = styles[.footer] {
            footer.style = style
        }
    }
}

public extension Refreshable where Self: UIScrollView {
    var refresher: MJRefresher? {
        if let refresher = objc_getAssociatedObject(self, &AssociatedKey.refresherkey) as? MJRefresher {
            return refresher
        }
        
        let refresher = MJRefresher(header: nil, footer: nil)
        
        objc_setAssociatedObject(self, &AssociatedKey.refresherkey, refresher, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        
        return refresher
    }
    
    func setupHeader(_ block: @escaping RefreshHeaderCallbakc) {
        mj_header = DuomNormalRefreshHeader { [weak self] in
            guard let refresher = self?.refresher else { return }
            block(refresher)
        }

        refresher?.header = mj_header
    }
    
    func setupFooter(_ block: @escaping RefreshFooterCallbakc) {
        mj_footer = DuomNormalRefreshFooter { [weak self] in
            guard let refresher = self?.refresher else { return }
            block(refresher)
        }
        
        refresher?.footer = mj_footer
    }
    
    func setupHeaderAndFooter(_ block: @escaping (RefreshType, Refresher) -> Void) {
        setupHeader { block(.header, $0) }
        setupFooter { block(.footer, $0)}
    }
    
//    func setupContinueLastHeader(_ block: @escaping RefreshHeaderCallbakc) {
//        mj_header = SHTContinueLastRefreshHeader { [weak self] in
//            guard let refresher = self?.refresher else { return }
//            block(refresher)
//        }
//
//        refresher?.header = mj_header
//    }
//
//    func setupContinueNextFooter(_ block: @escaping RefreshFooterCallbakc) {
//        mj_footer = SHTContinueNextRefreshFooter { [weak self] in
//            guard let refresher = self?.refresher else { return }
//            block(refresher)
//        }
//
//        refresher?.footer = mj_footer
//    }
}

private enum AssociatedKey {
    static var refresherkey: UInt8 = 0
}
