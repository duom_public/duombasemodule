//
//  Refresher.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import UIKit

public enum RefreshStyle {
    case dark
    case light
}

public enum RefreshType {
    case header
    case footer
}

public enum RefreshState {
    case empty
    case normal
    case noMore
}

public protocol Refresher: class {
    /// 是否正在刷新
    var isRefreshing: Bool { get }
    
    
    /// 设置刷新控件颜色样式
    /// - Parameters:
    ///   - style: 亮/暗
    ///   - type: 头部/尾部
    func setting(style: RefreshStyle, type: RefreshType)
    
    /// 通过状态结束刷新
    ///
    /// - Parameter state: 状态
    ///     - normal: 正常结束
    ///     -  empty: 空列表
    ///     - noMore: 没有更多数据
    /// - Note: 当 `state` 等于 `.empty` 或 `.noMore` 时， 如果存在底部加载控件，会重置底部控件状态到NoMore状态
    /// - Note: 当 `state` 等于 `.normal` 时，如果存在底部加载控件，会重置底部控件状态到Normal状态
    func endRefresh(with state: RefreshState)
    
    /// 手动开启刷新
    ///
    /// - Parameter type: 控件类型
    func beginRefresh(with type: RefreshType)
    
    /// 获取指定类型刷新控件，是否正在刷新
    ///
    /// - Parameter type: 控件类型
    /// - Returns: 是否正在刷新
    func isRefreshing(with type: RefreshType) -> Bool
}
