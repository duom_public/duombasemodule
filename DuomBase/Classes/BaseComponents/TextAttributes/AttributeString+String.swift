//
//  AttributeString+String.swift
//  Duom
//
//  Created by HK on 2020/2/17.
//  Copyright © 2020 butcheryl. All rights reserved.
//

import Foundation

// MARK: - Operations

/// Merge two attributed string in a single new attributed string.
///
/// - Parameters:
///   - lhs: attributed string.
///   - rhs: attributed string.
/// - Returns: new attributed string concatenation of two strings.
public func + (lhs: NSAttributedString, rhs: NSAttributedString) -> NSAttributedString {
    let final = NSMutableAttributedString(attributedString: lhs)
    final.append(rhs)
    return final
}

/// Merge a plain string with an attributed string to produce a new attributed string.
///
/// - Parameters:
///   - lhs: plain string.
///   - rhs: attributed string.
/// - Returns: new attributed string.
public func + (lhs: String, rhs: NSAttributedString) -> NSAttributedString {
    let final = NSMutableAttributedString(string: lhs)
    final.append(rhs)
    return final
}

/// Merge an attributed string with a plain string to produce a new attributed string.
///
/// - Parameters:
///   - lhs: attributed string.
///   - rhs: plain string.
/// - Returns: new attributed string.
public func + (lhs: NSAttributedString, rhs: String) -> NSAttributedString {
    let final = NSMutableAttributedString(attributedString: lhs)
    final.append(NSMutableAttributedString(string: rhs))
    return final
}
