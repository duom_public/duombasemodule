//
//  AttributedStringBuilder.swift
//  NiceTuanUIComponents
//
//  Created by 于磊 on 2021/9/14.
//

import Foundation

/*
 let attr = NSAttributedString {
     "title".attributes { $0.font(.appFont(ofSize: 14)) }

     if Bool.random() {
         "1. aaa".attributes { $0.font(.appFont(ofSize: 14)) }
     } else {
         "1. bbb".attributes { $0.font(.appFont(ofSize: 14)) }
     }

     for _ in 0 ... 3 {
         "\n"
     }

     "2. ccc".attributes { $0.font(.appFont(ofSize: 14)) }

     NSMutableAttributedString {
         "a. xxx".attributes { $0.font(.appFont(ofSize: 14)) }
         "b. xxx".attributes { $0.font(.appFont(ofSize: 14)) }
     }
 }
 */

@resultBuilder
public enum AttributedStringBuilder {
    public typealias Component = NSAttributedString?

    public static func buildBlock(_ components: Component...) -> Component {
        buildArray(components)
    }

    public static func buildExpression(_ component: Component) -> Component {
        return component
    }

    public static func buildExpression(_ text: String?) -> Component {
        return text.flatMap { NSAttributedString(string: $0) }
    }

    public static func buildOptional(_ component: Component) -> Component {
        return component
    }

    public static func buildEither(first component: Component) -> Component {
        return component
    }

    public static func buildEither(second component: Component) -> Component {
        return component
    }

    public static func buildArray(_ components: [Component]) -> Component {
        return components.compactMap { $0 }.reduce(into: NSMutableAttributedString()) { $0.append($1) }
    }
}

public extension NSAttributedString {
    convenience init(@AttributedStringBuilder _ builder: () -> NSAttributedString?) {
        self.init(attributedString: builder() ?? NSAttributedString())
    }
}
