//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import Foundation

public extension Array where Element: Equatable {
    func filterDuplicated() -> [Element] {
        var result = [Element]()
        for item in self {
            if !result.contains(item) {
                result.append(item)
            }
        }
        return result
    }
}
