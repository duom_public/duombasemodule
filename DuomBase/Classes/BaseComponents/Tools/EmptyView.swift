//
//  EmptyView.swift
//  DuomBase
//
//  Created by kuroky on 2022/10/19.
//

import Foundation

public class EmptyView: UIView {
    public enum EmptyViewType {
        case normal(title: String?)
        case liveRoom(title: String?)
        case chatRoom(title: String?)
        case sticker(title: String?)
        case emptyButton(title: String?)
    }
    
    public var customFilter: FilterHandler?
    
    public var edgeInsets: UIEdgeInsets = .zero
    
    private var imageView = UIImageView(image: UIImage(named: "page_empty")).then {
        $0.contentMode = .scaleToFill
    }
    
    private var titleLabel = UILabel().then {
        $0.font = .appFont(ofSize: 15, fontType: .Inter_Medium)
        $0.textColor = UIColor(217, 217, 217)
        $0.textAlignment = .center
    }
    
    private var doButton = DuomThemeButton.init(style: .highLight, title: "")
    
    private var contentView = UIView().then {
        $0.backgroundColor = .clear
    }
    
    public convenience init(_ type: EmptyViewType) {
        self.init()
        update(by: type)
    }
    
    public init() {
        super.init(frame: .zero)
        setUpView()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("empty init coder has not been implemented")
    }
    
    private func setUpView()  {
        addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(doButton)
        
        contentView.snp.makeConstraints { make in
            make.height.greaterThanOrEqualTo(0).priority(.low)
            make.width.equalToSuperview()
            make.center.equalToSuperview()
        }
        
        imageView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.size.equalTo(CGSize(width: 125.DScale, height: 150.DScale))
            make.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.top.equalTo(imageView.snp.bottom)
            make.centerX.equalToSuperview()
            make.height.equalTo(15.DScale)
        }
        
        doButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 195, height: 38))
        }
    }
    
}

extension EmptyView: EmptyAble {
    private func update(by type: EmptyViewType)  {
        switch type {
        case let .normal(title: title):
            titleLabel.text = title ?? "NO Data"
            doButton.isHidden = true
        case .liveRoom(let title):
            titleLabel.text = title ?? "NO Data"
            doButton.isHidden = true
        case .chatRoom(let title):
            titleLabel.text = title ?? "NO Data"
            doButton.isHidden = true
        case .sticker(let title):
            titleLabel.text = title ?? " You don't have a sticker"
            titleLabel.textColor = .white
            titleLabel.font = .appFont(ofSize: 14)
            imageView.image = UIImage(named: "empty_sticker")
            
            imageView.snp.updateConstraints { make in
                make.size.equalTo(CGSize(width: 150, height: 150))
            }
            doButton.isHidden = true
        case .emptyButton(let title):
            doButton.isHidden = false
            titleLabel.isHidden = true
            imageView.isHidden = true
            doButton.setTitle(title, for: .normal)
            doButton.setTitle(title, for: .highlighted)
        }
    }
}
