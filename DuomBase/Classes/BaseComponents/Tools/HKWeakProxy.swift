//
//  HKWeakProxy.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/5.
//

import Foundation

public class HKWeakProxy: NSObject {
    
    private weak var target: NSObjectProtocol?
    
    public init(target: NSObjectProtocol) {
        self.target = target
        super.init()
    }
    
    public class func proxy(withTarget target: NSObjectProtocol) -> HKWeakProxy {
        return HKWeakProxy(target: target)
    }
    
    public override func forwardingTarget(for aSelector: Selector!) -> Any? {
        return target
    }
    
    public override func responds(to aSelector: Selector!) -> Bool {
        return target?.responds(to: aSelector) ?? false
    }
    
}
