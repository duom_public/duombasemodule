//
//  IconTextView.swift
//  Duom
//
//  Created by kuroky on 2022/8/26.
//

import UIKit
import Foundation

public class IconTextView: UIControl {
    public private(set) var label = UILabel(frame: CGRect.zero)
    public private(set) var imageView = UIImageView(frame: CGRect.zero)
    
    public var offsetLeftX: CGFloat = 0
    public var offsetRightX: CGFloat = 0
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setUpUI()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpUI()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpUI()  {
        self.backgroundColor = UIColor(red: 0,
                                       green: 0,
                                       blue: 0,
                                       alpha: 0.4)
        label.textAlignment = .right
        
        self.addSubview(imageView)
        self.addSubview(label)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        let height = self.frame.height
        let width = self.frame.width
        let radius = height * 0.5
        self.layer.cornerRadius = radius
        
        let subsTopSpace: CGFloat = 2.0
        let imageViewHeight = height - (subsTopSpace * 2.0)
        let imageViewWidth = imageViewHeight
        let imageX = radius + offsetLeftX
        let imageY = subsTopSpace
        
        let imageViewFrame = CGRect(x: imageX,
                                    y: imageY,
                                    width: imageViewWidth,
                                    height: imageViewHeight)
        imageView.frame = imageViewFrame
        
        let labelHeight = imageViewHeight
        let labelWidth = width - radius - imageViewFrame.maxX + offsetRightX
        let labelX = imageViewFrame.maxX
        let labelY = subsTopSpace
        
        let labelFrame = CGRect(x: labelX,
                                y: labelY,
                                width: labelWidth,
                                height: labelHeight)
        label.frame = labelFrame
    }
}
