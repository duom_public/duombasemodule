//
//  Color+Extension.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import Foundation

public enum LogLevel {
    case info, warning, error
    var description: String {
        switch self {
        case .info:    return "Info 🍺"
        case .warning: return "Warning 🤔"
        case .error:   return "Error 😭"
        }
    }
}

public class Logger {
    public static var enable: Bool = true
    public static func log(message: String, level: LogLevel) {
        #if !DEBUG
        if !enable {
            return
        }
        #endif
        print("\(level.description): \(message)")
    }
}
