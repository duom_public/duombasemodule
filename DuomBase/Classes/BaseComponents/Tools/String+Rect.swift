//
//  String+Rect.swift
//  DuomBase
//
//  Created by kuroky on 2022/11/29.
//

import Foundation

extension String {
    public func boundingRect(font: UIFont, maxSize: CGSize) -> CGSize {
        let style = NSMutableParagraphStyle()
        style.lineBreakMode = .byCharWrapping
        
        let att = [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: style]
        
        let attContent = NSMutableAttributedString(string: self, attributes: att)
        
        let size = attContent.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).size
        
        return CGSize(width: ceil(size.width), height: ceil(size.height))
    }
    
    // 判断是否是gif
    public func isGif() -> Bool {
        return lowercased().hasSuffix("gif")
    }
}
