//
//  UIView+Gradient.swift
//  DuomBase
//
//  Created by kuroky on 2022/12/27.
//

import Foundation

extension UIView {
    @discardableResult
    public func gradient(colors: [UIColor],
                  point: (CGPoint, CGPoint) = (CGPoint(x: 0.5, y: 0), CGPoint(x: 0.5, y: 1)),
                  locations: [NSNumber] = [0, 1],
                  frame: CGRect = .zero,
                  radius: CGFloat = 0,
                  at: UInt32 = 0) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.locations = locations
        if frame == .zero {
            gradientLayer.frame = self.bounds
        } else {
            gradientLayer.frame = frame
        }
        gradientLayer.startPoint = point.0
        gradientLayer.endPoint = point.1
        gradientLayer.cornerRadius = radius
        self.layer.insertSublayer(gradientLayer, at: at)
        return gradientLayer
    }
}
