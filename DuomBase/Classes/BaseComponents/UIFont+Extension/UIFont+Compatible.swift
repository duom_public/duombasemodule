//
//  UIFont+Compatible.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import UIKit

public enum FontType: String {
    case GT = "GT Flexa Trial"
    case GT_Bold = "GTFlexaTrial-Bd"
    case Inter_Blod = "Inter-Bold"
    case Inter_Medium = "Inter-Medium"
    case CR_Regular = "CoolveticaRg-Regular"
    case CL_Regular = "CoolveticaLt-Regular"
    case TT_Bold = "TTCommons-Bold"
    case TT_Regular = "TTCommons-Regular"
    case TT_DemiBold = "TTCommons-DemiBold"
}

public extension UIFont {
    static var fontSizeMultiplier: CGFloat = min(1.5, max(1.f, UIScreen.screenWidth / 375.f))
    
    static func appFont(ofSize size: CGFloat, fontType: FontType = .Inter_Medium) -> UIFont {
        if let font = UIFont(name: fontType.rawValue, size: size) {
            return font
        } else {
            return .systemFont(ofSize: size)
        }
    }
    
    static func logFontNames() {
        for fontFamilyName in familyNames {
            print("familyNames: \(fontFamilyName)")
            for fontName in fontNames(forFamilyName: fontFamilyName) {
                print(">>>>>fontName: \(fontName)")
            }
        }
        print("end>>>>>>>>>>>>>>")
    }
    
    static func appFont(ofScaleBase size: CGFloat, fontType: FontType = .Inter_Medium) -> UIFont {
        let font = UIFont.appFont(ofSize: size, fontType: fontType)
        return UIFont(descriptor: font.fontDescriptor, size: ceil(font.pointSize * fontSizeMultiplier))
    }
    
//    static func getCustomFont(with path: String, size: CGFloat, fontName: String, fontSourceType: String) -> UIFont {
//        let fontUrl = URL(string: Bundle(url: Bundle.main.url(forResource: "fonts", withExtension: "bundle")!)!.path(forResource: fontName, ofType: fontSourceType)!)
//        let fontData = CGDataProvider(url: fontUrl! as CFURL)
//        let fontRef = CGFont(fontData!)
//        CTFontManagerRegisterGraphicsFont(fontRef!, nil)
//        let fontName = fontRef?.postScriptName as! String
//        let font = UIFont(name: fontName, size: size)
//        return font!
//    }
}

