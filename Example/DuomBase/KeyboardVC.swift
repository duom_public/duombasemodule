//
//  KeyboardVC.swift
//  DuomBase_Example
//
//  Created by kuroky on 2023/2/15.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import Foundation
import DuomBase

class KeyboardVC: UIViewController, UITextFieldDelegate {
    
    private let textField: UITextField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(textField)
        textField.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 300, height: 80))
        }
        textField.delegate = self
        textField.backgroundColor = .purple
        
        textField.inputView = TestInputView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: 324))
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textField.resignFirstResponder()
    }
    
}


class TestInputView: BaseView {
    private let containerView: UIView = UIView().then {
        $0.backgroundColor = .red
    }
    override func setupUI() {
        super.setupUI()
        addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    
}
