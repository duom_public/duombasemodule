//
//  MessageVC.swift
//  DuomBase_Example
//
//  Created by kuroky on 2023/9/1.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class MessageVC: UIViewController, MFMessageComposeViewControllerDelegate {
    
    let doubleBtn: UIButton = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(doubleBtn)
        doubleBtn.backgroundColor = .yellow
        doubleBtn.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 300, height: 200))
        }
        
        doubleBtn.addTarget(self, action: #selector(doubleACtion(_:)), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissAction(_:)), name: NSNotification.Name(rawValue: "dismiss"), object: nil)
    }
    
    @objc func dismissAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    
    @objc func doubleACtion(_ sender: UIButton) {
        shareMessage()
    }
    
    
    
    func shareMessage()  {
        if MFMessageComposeViewController.canSendAttachments() {
            let vc = MFMessageComposeViewController()
            vc.recipients = ["+8618629414813"]
            vc.body = "哈喽沃德"
            vc.messageComposeDelegate = self
            UIApplication.topViewController()?.present(vc, animated: true)
        }
    }
}


extension MessageVC {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true)
        switch result {
        case .cancelled:
            print("1")
        case .sent:
            print("2")
        case .failed:
            print("3")
        }
    }
}
