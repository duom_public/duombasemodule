//
//  SecVC.swift
//  DuomBase_Example
//
//  Created by kuroky on 2022/11/16.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import UIKit

class SecVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .blue
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let fir = FirVC()
//        self.navigationController?.pushViewController(fir, animated: true)
        
        NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "dismiss")))
    }
}
