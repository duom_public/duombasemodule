////
////  VideoVC.swift
////  DuomBase_Example
////
////  Created by kuroky on 2023/2/22.
////  Copyright © 2023 CocoaPods. All rights reserved.
////
//
//import Foundation
//import UIKit
//import DuomBase
//import YYEVA
//
//class VideoVC: UIViewController {
//    
//    let player = YYEVAPlayer()
////
////    lazy var  metalView = {
////        let me = BDAlphaPlayerMetalView(delegate: self)
////        return me
////    }()
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        view.backgroundColor = .black
//        
//        let btn1 = UIButton()
//        btn1.setTitle("库1", for: .normal)
//        view.addSubview(btn1)
//        
//        let btn2 = UIButton()
//        btn2.setTitle("库2", for: .normal)
//        view.addSubview(btn2)
//        
//        btn1.snp.makeConstraints { make in
//            make.leading.equalToSuperview()
//            make.top.equalTo(100)
//            make.size.equalTo(CGSize(width: 100, height: 80))
//        }
//        
//        btn2.snp.makeConstraints { make in
//            make.trailing.equalToSuperview()
//            make.top.equalTo(100)
//            make.size.equalTo(CGSize(width: 100, height: 80))
//        }
//        
//        btn1.addTarget(self, action: #selector(self.btn1Action), for: .touchUpInside)
//        btn2.addTarget(self, action: #selector(self.btn2Action), for: .touchUpInside)
//    }
//    
//    @objc func btn1Action()  {
//        let file = Bundle.main.path(forResource: "中级礼物", ofType: "mp4")
//        
//        self.view.addSubview(player)
//        player.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
//        }
//        player.play(file ?? "")
//    }
//    
//    @objc func btn2Action()  {
//////        NSString *testResourcePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"TestResource"];
//////        NSString *directory = [testResourcePath stringByAppendingPathComponent:@"heartbeats"];
//////        let afile = Bundle.main.bundlePath.stringByApp("TestResource")
////        let file = Bundle.main.path(forResource: "heartbeats", ofType: "mp4")
////
////        self.view.addSubview(metalView)
//////        metalView.snp.makeConstraints { make in
//////            make.edges.equalToSuperview()
//////        }
////
////        let config = BDAlphaPlayerMetalConfiguration.default()
////        config.directory = file ?? ""
////        config.renderSuperViewFrame = self.view.frame
////        config.orientation = .portrait
////
////        metalView.play(with: config)
//        
//    }
//}
//
//extension VideoVC: IYYEVAPlayerDelegate {
//    func evaPlayerDidCompleted(_ player: YYEVAPlayer) {
//        player.removeFromSuperview()
//    }
//}
//
