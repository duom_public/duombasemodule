//
//  ViewController.swift
//  DuomBase
//
//  Created by HeKai_Duom on 08/25/2022.
//  Copyright (c) 2022 HeKai_Duom. All rights reserved.
//

import UIKit
import DuomBase
import RxSwift
import RxCocoa

class ViewController: UIViewController, ViewType, ResponderChainRouter {
    typealias Event = TViewModel.Action

    func bind(viewModel: TViewModel) {
        
    }
    
    typealias TargetViewModel = TViewModel
    
    var disposeBag = DisposeBag()
     
    let firVC = FirVC()
    
    let tView = TTView()

//    private lazy var layout = UICollectionViewFlowLayout().then {
//        $0.itemSize = CGSize(width: 50, height: 50)
//        $0.minimumLineSpacing = 10
//        $0.minimumLineSpacing = 10
//    }
//
//    private lazy var colletionView: UICollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let label = UILabel().then {
            $0.font = .appFont(ofSize: 30, fontType: .GT)
            $0.text = "1234567890"
        }
        view.addSubview(label)
        label.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 200, height: 100))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Logger.log(message: "error: test", level: .info)
        
        let alert = DuomAlertCardViewController(title: "Discard edits?", content: "If you go back now, you will lose all the edit you’ve made.")
        UIApplication.topViewController()?.present(alert, animated: true)
    }

}


class TTView: BaseView {
    let btn = UIButton().then {
        $0.backgroundColor = .yellow
    }
    
    let btn2 = UIButton().then {
        $0.backgroundColor = .brown
    }
    
    let btn3 = UIButton().then {
        $0.backgroundColor = .purple
    }
    
    
    let disbag = DisposeBag()
    
    override func setupUI() {
        addSubview(btn)
        btn.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth / 3)
        }
        
        addSubview(btn2)
        btn2.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth / 3)
            make.leading.equalTo(btn.snp.trailing)
        }
        
        addSubview(btn3)
        btn3.snp.makeConstraints { make in
            make.trailing.top.bottom.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth / 3)
        }
        
        btn.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.emitRouterEvent(ViewController.Event.testA)
            })
            .disposed(by: disbag)
        
        btn2.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.emitRouterEvent(ViewController.Event.testB)
            })
            .disposed(by: disbag)
        
        btn3.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.emitRouterEvent(ViewController.Event.testB)
            })
            .disposed(by: disbag)
    }
    
    
}
