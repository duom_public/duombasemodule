# DuomBase

[![CI Status](https://img.shields.io/travis/HeKai_Duom/DuomBase.svg?style=flat)](https://travis-ci.org/HeKai_Duom/DuomBase)
[![Version](https://img.shields.io/cocoapods/v/DuomBase.svg?style=flat)](https://cocoapods.org/pods/DuomBase)
[![License](https://img.shields.io/cocoapods/l/DuomBase.svg?style=flat)](https://cocoapods.org/pods/DuomBase)
[![Platform](https://img.shields.io/cocoapods/p/DuomBase.svg?style=flat)](https://cocoapods.org/pods/DuomBase)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DuomBase is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DuomBase'
```

## Author

HeKai_Duom, kai.he@duom.com

## License

DuomBase is available under the MIT license. See the LICENSE file for more info.
